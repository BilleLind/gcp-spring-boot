package com.example.gcpspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class GcpSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(GcpSpringbootApplication.class, args);
    }



}
